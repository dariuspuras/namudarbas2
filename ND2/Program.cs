﻿using System;

namespace ND2
{
    class Program
    {
        static string kalba;
        static string vardas;
        static string pavarde;
        static int amzius;
        static float ugis;
        static string moteris;
        static string mokykla;
        static int klase;
        static int menesiai;
        static string baigetemokykla;
        static string istojotemokytis;
        static string kuristojotemokytis;
        static int metai;
        static string gyvenaVilniuje;
        static string rajonas;
        static string kitasmiestas;
        static string vaikai;
        static int vaikukiekis;
        static string santuoka;
        static string komentaras;
        static string vaikai1;
        static string anukai;
        static int mergaites;
        static int berniukai;
        static int anukaimergaites;
        static int anukaiberniukai;
        static string sportas;
        static int sportokartai;

        static void Main(string[] args)
        {
            Console.WriteLine("*** Namu Darbas2 ***");
            Console.WriteLine();

            Console.WriteLine("Prasau pasirinkite kalba:");
            Console.WriteLine("1 - lietuviu");
            Console.WriteLine("2 - anglu");
            Console.WriteLine("3 - rusu");

            kalba = Console.ReadLine();

            if (kalba == "1")
            {
                Console.WriteLine("Sveiki!");
            }

            if (kalba == "2")
            {
                Console.WriteLine("Hello!");
            }

            if (kalba == "3")
            {
                Console.WriteLine("Privet!");
            }

            Console.WriteLine("Prasau iveskite varda");
            vardas = Console.ReadLine();

            if (ArPabaiga(vardas) == true)
            {
                goto Pabaiga;
            }

            Console.WriteLine("Prasau iveskite pavarde");
            pavarde = Console.ReadLine();

            if (ArPabaiga(pavarde) == true)
            {
                goto Pabaiga;
            }

            Console.WriteLine("Prasau iveskite amziu");
            string amz = Console.ReadLine();
            int.TryParse(amz, out amzius);

            if (ArPabaiga(amz) == true)
            {
                goto Pabaiga;
            }

            Console.WriteLine("Prasau ivesti ugi");
            string ug = Console.ReadLine();
            float.TryParse(ug, out ugis);

            if (ArPabaiga(ug) == true)
            {
                goto Pabaiga;
            }

            TaipNeKlausimas("Ar esate moteris?");
            moteris = Console.ReadLine();

            bool arMoteris;

            if (moteris == "1")
            {
                arMoteris = true;
            }

            if (moteris == "2")
            {
                arMoteris = false;
            }

            if (ArPabaiga(moteris) == true)
            {
                goto Pabaiga;
            }

            if (vardas != "" && pavarde != "" && amzius > 0 && ugis > 0.30)
            {
                if (amzius > 0 && amzius <= 10)
                {
                    TaipNeKlausimas("Ar einate i mokykla?");
                    mokykla = Console.ReadLine();

                    bool arEinateIMokykla;

                    if (mokykla == "1")
                    {
                        arEinateIMokykla = true;

                        Console.WriteLine("I kuria klase einate?");
                        string klas = Console.ReadLine();
                        int.TryParse(klas, out klase);

                        if (ArPabaiga(klas) == true)
                        {
                            goto Pabaiga;
                        }
                    }

                    if (mokykla == "2")
                    {
                        arEinateIMokykla = false;

                        Console.WriteLine("Kiek dar mėnesių liko laukti kol pradėsi eiti į mokyklą?");
                        string men = Console.ReadLine();
                        int.TryParse(men, out menesiai);

                        if (ArPabaiga(men) == true)
                        {
                            goto Pabaiga;
                        }
                    }

                    if (ArPabaiga(mokykla) == true)
                    {
                        goto Pabaiga;
                    }

                }

                if (amzius > 10 && amzius <= 20)
                {
                    TaipNeKlausimas("Ar baigete mokykla?");
                    baigetemokykla = Console.ReadLine();

                    bool arBaigeteMokykla;

                    if (baigetemokykla == "1")
                    {
                        arBaigeteMokykla = true;

                        TaipNeKlausimas("Ar istojote mokytis?");
                        istojotemokytis = Console.ReadLine();

                        bool arIstojoteMokytis;

                        if (istojotemokytis == "1")
                        {
                            arIstojoteMokytis = true;

                            Console.WriteLine("Kur įstojote mokytis?");
                            kuristojotemokytis = Console.ReadLine();
                        }

                        if (istojotemokytis == "2")
                        {
                            arIstojoteMokytis = false;
                        }

                        if (ArPabaiga(istojotemokytis) == true)
                        {
                            goto Pabaiga;
                        }
                    }

                    if (mokykla == "2")
                    {
                        arBaigeteMokykla = false;

                        Console.WriteLine("Kiek dar metų liko mokytis mokykloje?");
                        string met = Console.ReadLine();
                        int.TryParse(met, out metai);

                        if (ArPabaiga(met) == true)
                        {
                            goto Pabaiga;
                        }
                    }

                    if (ArPabaiga(baigetemokykla) == true)
                    {
                        goto Pabaiga;
                    }
                }

                if (amzius > 20 && amzius <= 30)
                {
                    TaipNeKlausimas("Ar gyvenate Vilniuje?");
                    gyvenaVilniuje = Console.ReadLine();

                    bool arGyvenateVilniuje;

                    if (gyvenaVilniuje == "1")
                    {
                        arGyvenateVilniuje = true;

                        Console.WriteLine("Kuriame rajone gyvenate?");
                        rajonas = Console.ReadLine();

                        if (ArPabaiga(rajonas) == true)
                        {
                            goto Pabaiga;
                        }

                    }

                    if (gyvenaVilniuje == "2")
                    {
                        arGyvenateVilniuje = false;

                        Console.WriteLine("Iveskite kita miestą, kuriame gyvenate?");
                        kitasmiestas = Console.ReadLine();

                        if (ArPabaiga(kitasmiestas) == true)
                        {
                            goto Pabaiga;
                        }
                    }

                    if (ArPabaiga(gyvenaVilniuje) == true)
                    {
                        goto Pabaiga;
                    }
                }

                if (amzius > 30 && amzius <= 40)
                {
                    TaipNeKlausimas("Ar turite vaikų?");
                    vaikai = Console.ReadLine();

                    bool arTuriteVaiku;

                    if (vaikai == "1")
                    {
                        arTuriteVaiku = true;

                        Console.WriteLine("Kiek vaikų turite?");
                        string vaikkiek = Console.ReadLine();
                        int.TryParse(vaikkiek, out vaikukiekis);

                        if (ArPabaiga(vaikkiek) == true)
                        {
                            goto Pabaiga;
                        }

                    }

                    if (vaikai == "2")
                    {
                        arTuriteVaiku = false;
                    }

                    if (ArPabaiga(vaikai) == true)
                    {
                        goto Pabaiga;
                    }
                }

                if (amzius > 40 && amzius <= 50)
                {
                    TaipNeKlausimas("Ar Jūsų nuomonė apie tos pačios lyties santuokas yra teigiama?");
                    santuoka = Console.ReadLine();

                    bool Santuoka;

                    if (santuoka == "1")
                    {
                        Santuoka = true;
                    }

                    if (santuoka == "2")
                    {
                        Santuoka = false;

                        Console.WriteLine("Įveskite papildomą komentarą?");
                        komentaras = Console.ReadLine();
                    }

                    if (ArPabaiga(vaikai) == true)
                    {
                        goto Pabaiga;
                    }
                }

                if (amzius > 51 && amzius <= 70)
                {
                    TaipNeKlausimas("Ar turite vaikų?");

                    bool arTuriteVaiku1;

                    if (vaikai1 == "1")
                    {
                        arTuriteVaiku1 = true;

                        Console.WriteLine("Kiek vaikų mergaičių turite?");
                        string merg = Console.ReadLine();
                        int.TryParse(merg, out mergaites);

                        if (ArPabaiga(merg) == true)
                        {
                            goto Pabaiga;
                        }

                        Console.WriteLine("Kiek vaikų berniukų turite?");
                        string bern = Console.ReadLine();
                        int.TryParse(bern, out berniukai);

                        if (ArPabaiga(bern) == true)
                        {
                            goto Pabaiga;
                        }

                        TaipNeKlausimas("Ar turite anūkų?");

                        bool arTuriteAnuku;

                        if (anukai == "1")
                        {
                            arTuriteAnuku = true;

                            Console.WriteLine("Kiek anūkų mergaičių turite?");
                            string anukaimerg = Console.ReadLine();
                            int.TryParse(anukaimerg, out anukaimergaites);

                            if (ArPabaiga(anukaimerg) == true)
                            {
                                goto Pabaiga;
                            }

                            Console.WriteLine("Kiek anūkų berniukų turite?");
                            string anukaibern = Console.ReadLine();
                            int.TryParse(anukaibern, out anukaiberniukai);

                            if (ArPabaiga(anukaibern) == true)
                            {
                                goto Pabaiga;
                            }
                        }

                        if (anukai == "2")
                        {
                            arTuriteAnuku = false;
                        }

                        if (ArPabaiga(anukai) == true)
                        {
                            goto Pabaiga;
                        }
                    }

                    if (vaikai1 == "2")
                    {
                        arTuriteVaiku1 = false;
                    }

                    if (ArPabaiga(vaikai1) == true)
                    {
                        goto Pabaiga;
                    }
                }

                if (amzius > 70)
                {
                    TaipNeKlausimas("Ar vis dar sportuojate?");

                    bool arSportuojate;

                    if (sportas == "1")
                    {
                        arSportuojate = true;

                        Console.WriteLine("Kiek kartų per savaitę sportuojate?");
                        string sportokart = Console.ReadLine();
                        int.TryParse(sportokart, out sportokartai);

                        if (ArPabaiga(sportokart) == true)
                        {
                            goto Pabaiga;
                        }
                    }

                    if (sportas == "2")
                    {
                        arSportuojate = false;
                    }

                    if (ArPabaiga(sportas) == true)
                    {
                        goto Pabaiga;
                    }
                }

                Informacija();
            }
            else
            {
                Console.WriteLine("Netinkamai ivesti duomenys");
            }

        Pabaiga:
            Console.WriteLine("Programos pabaiga");


            Console.ReadLine();

        }

        static void TaipNeKlausimas(string klausimas)
        {
            Console.WriteLine("-----------------");
            Console.WriteLine(klausimas);
            Console.WriteLine("1 - Taip");
            Console.WriteLine("2 - Ne");
        }

        static void Informacija()
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("Tnformacija:");
            Console.WriteLine("Vardas: " + vardas);
            Console.WriteLine("Amzius: " + amzius);
            Console.WriteLine("Ugis: " + ugis);
        }

        static bool ArPabaiga(string kintamasis)
        {
            if (kalba == "1" && kintamasis == "ate" || kalba == "2" && kintamasis == "exit" || kalba == "3" && kintamasis == "dosvidanija")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
